#! /usr/bin/env bash

#set -x #debugging Mode on
set -u
#set -e

#######################################################
############### L O G   P A T H #######################
#######################################################
LOG_PATH=${LOG_PATH-"<CHANGE-ME>"}
#######################################################

#######################################################
############### B A C K U P  P A T H ##################
#######################################################
BACKUP_DIR=${BACKUP_DIR-"CHANGE-ME>"}
#######################################################

#######################################################
############### D E S T I N A T I O N #################
#######################################################
DEST=${DEST-"CHANGE-ME>"}
#######################################################


PATH="/sbin/:/bin/:/usr/sbin/:/usr/bin/"
IFS='
	 '
PROGNAME=`basename $0 .sh`
ARCHIVE_FILE=${BACKUP_DIR##*/}
BACKUP_TIME=`date +"%m-%d-%Y-%H-%M"`

LOG_FILE=$(mktemp "$LOG_PATH/${PROGNAME}.XXXXXX")
exec 3>> $LOG_FILE #open $LOG_FILE for writing(append) 

EXIT_FAILURE=1
EXIT_SUCCESS=0


fatal(){
	printf "[%s]: %s\n" "$PROGNAME" "$2" 
	exit $1
}

#error handling(check file|directory exist or Not)
[ -d $BACKUP_DIR ] || fatal "$EXIT_FAILURE" "No such file or directory \"$BACKUP_DIR\" "
[ -d $DEST ] || fatal "$EXIT_FAILURE" "No such file or directory \"$DEST\" " 

#declare array variable
declare -a PREV_FILES

#save name of previous file(s)
save_prev(){
	local i=1
	for file in $(echo $DEST/*);do 
		if [[ $file =~ ^.*$1.*$ ]];then
			PREV_FILES[i]="$file"
		fi
		i=$((i+1))
	done
	return
}
#delete previous file(s)
del_prev(){
	for file in ${PREV_FILES[@]};do
		rm -f ${file:?"err_env!"} 2>&3
	done
	return
}

dailyBackup(){
	(cd $BACKUP_DIR && tar cpzfP "${DEST}/${ARCHIVE_FILE}-$BACKUP_TIME-daily.tar" * 2>&3)
	return
}
weeklyBackup(){
	(cd $BACKUP_DIR && tar cpzfP "${DEST}/${ARCHIVE_FILE}-$BACKUP_TIME-weekly.tar" * 2>&3)
	return
}

#Main
case ${1:-"d"} in #default daily
	daily|d)
		save_prev "daily"
		dailyBackup
			if [ $? -eq 1 ];then
				exit $EXIT_FAILURE
			else
				del_prev 
			fi; ;;
	weekly|w)
		save_prev "weekly"
		weeklyBackup 
			if [ $? -eq 1 ];then
				exit $EXIT_FAILURE
			else
				del_prev
			fi; ;;
	*)
		fatal "$EXIT_FAILURE" "invalid argument"
esac

exec 3>&-
set +u
#set +e
exit $EXIT_SUCCESS

#set +x Debugging Mode Off

